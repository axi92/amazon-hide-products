// ==UserScript==
// @name         Amazon-hide-products
// @namespace    [[bitbucket reposetory url]]
// @version      0.1
// @description  Hide products that you dont want to see in your search results
// @author       axi92
// @include        https://*.amazon.de*
// @include        http://*.amazon.de*
// @match          https://*.amazon.de*
// @match          http://*.amazon.de*
// @require      http://code.jquery.com/jquery-latest.js
// @grant        none
// @run-at document-end
// ==/UserScript==

$ = jQuery.noConflict(true);
var pageContentchanged = false;
$('body').bind("DOMSubtreeModified", function() {
    pageContentchanged = true;
});
setInterval(removeSponsoredAds, 200);
console.log("amazon-sponsored-items-blocker loaded");

if (localStorage.getItem("hide_products") === null) {
    var hide_products = [];
}else{
    hide_products = JSON.parse(localStorage.getItem("hide_products"));
}
console.log("Hidden product IDs: ");
console.log(hide_products);


//add the Hide text to every search result
$( ".a-row .a-spacing-large" ).append( "<span class=\"clicktohide\">Hide</span>" );
$( ".a-row .a-badge-region" ).append( "<span class=\"clicktohide\">Hide</span>" ); //Amazons choice
$( ".a-row .a-spacing-large" ).css('text-align', '-webkit-right');
$( ".a-row .a-badge-region" ).css('text-align', '-webkit-right'); //Amazons choice

(function( hide_products ) {
    $('.clicktohide').on('click',function(){
        var product_id = $(this).parents("[data-asin]:first").attr("data-asin");
        hide_products.push(product_id);
        localStorage.setItem("hide_products", JSON.stringify(hide_products));
        //$(this).hide();
        $(this).children().css({ 'opacity' : 0.25 }).end().css({ 'opacity' : 0.25 });
    });
})( hide_products );

function arrayContains(needle, arrhaystack)
{
    return (arrhaystack.indexOf(needle) > -1);
}

function removeSponsoredAds() {
    //https://github.com/Stefan-Code/amazon-sponsored-items-blocker
    if (pageContentchanged) {
        var ad_count = 0;
        var hide_count = 0;
        $('.celwidget').each(function(i, obj) {
            if ($(this).find(".s-sponsored-info-icon").length > 0) {
                //console.log("Object " + i + " contains an ad");
                //$(this).css('background-color', 'red');
                //(this).remove();
                ad_count++;
            }
            if ($(this).attr("data-asin")){
                if ($(this).attr("data-asin").length > 0) {
                    //console.log($(this).attr("data-asin"));
                    if ( arrayContains($(this).attr("data-asin"), hide_products) ){
                        //$(this).css('background-color', 'blue');
                        //$(this).hide();
                        $(this).children().css({ 'opacity' : 0.25 }).end().css({ 'opacity' : 0.25 });
                        hide_count++;
                    }
                }
            }
        });
        //console.log("amazon-sponsored-items-blocker: " + ad_count + " ads removed!");
        console.log("Amazon hide products: " + hide_count + " elements are hidden!");
        pageContentchanged = false;
    }
}